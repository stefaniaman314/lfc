﻿#pragma once

#include <vector>
#include <string>

using namespace std;

class Grammar
{
public:
	using Production = pair<string, string>;

public:
	Grammar();
	Grammar(vector<char> VN, vector<char> VT, char S, vector<Production> P);
	~Grammar();

public:
	//Getters and Setters
	char GetS() const;
public:
	//Cititrea elementelor gramaticii.
	void readFromFile(const string& fileName, const int& sizeVN, const int& sizeVT, const int& sizeP);

	//Afisarea elementelor gramaticii.
	void printGrammar();

	//Verificarea corectitudinii gramaticii
	bool verifyGrammar();

	//Generarea de cuvinte pornind de la simbolul de start.
	//Dacă opțiunea este 0 se afișază doar cuvântul obținut.
	//Daca opțiunea este 1 se afișază și pașii intermediari.
	void generateWord(string word, const bool& option);

	//Functie care verifica daca cuvantul obtinut este format doar din terminale
	bool verifyWord(string word);

	//Functie ajutatoare pentru generarea random a unei productii aplicabile
	Production getRandomProduction(vector<Production> productions);

	//Functie ajutatoare pentru generarea productiilor aplicabile pe un anumit cuvant
	vector<Production> getApplicableProductions(string word);


private:
	vector<char> m_VN;
	vector<char> m_VT;
	char m_S;
	vector<Production> m_P;
};

