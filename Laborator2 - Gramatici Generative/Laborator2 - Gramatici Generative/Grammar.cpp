#include <iostream>
#include <fstream>
#include <random>
#include <algorithm>

#include "Grammar.h"

Grammar::Grammar()
{
	//
}

Grammar::Grammar(vector<char> VN, vector<char> VT, char S, vector<Production> P) :
	m_VN(VN),
	m_VT(VT),
	m_S(S),
	m_P(P)
{
	//
}

Grammar::~Grammar()
{
	//
}

void Grammar::readFromFile(const string& fileName, const int& sizeVN, const int& sizeVT, const int& sizeP)
{
	ifstream myFile;
	myFile.open("fileIn.txt");

	while (!myFile.eof())
	{
		//read VN
		for (auto index = 0; index < sizeVN; ++index)
		{
			char elemVN;
			myFile >> elemVN;
			m_VN.push_back(elemVN);
		}
		

		//read VT
		for (auto index = 0; index < sizeVT; ++index)
		{
			char elemVT;
			myFile >> elemVT;
			m_VT.push_back(elemVT);
		}

		//read S
		myFile >> m_S;

		//read P
		for (auto index = 0; index < sizeP; ++index)
		{
			Production prod;
			myFile >> prod.first >> prod.second;
			m_P.push_back(prod);
		}
		
	}

	myFile.close();
}

void Grammar::printGrammar()
{
	//print VN
	cout << "VN = { ";
	for (auto index = 0; index < m_VN.size(); ++index)
		cout << m_VN[index] << " ";
	cout << "} \n";

	//print VT
	cout << "VT = { ";
	for (auto index = 0; index < m_VT.size(); ++index)
		cout << m_VT[index] << " ";
	cout << "} \n";

	//print S
	cout << "S = " << m_S << "\n";

	//print P
	cout << "P : \n { \n";
	for (auto index = 0; index < m_P.size(); ++index)
		cout << '\t' << m_P[index].first << " -> " << m_P[index].second << '\n';
	cout << " } \n";
}

bool Grammar::verifyGrammar()
{
	bool verify = true;

	//1. Verificam daca VN si VT sunt disjuncte
	for (auto it = m_VN.begin(); it != m_VN.end(); ++it)
	{
		if (find(m_VT.begin(), m_VT.end(), *it) != m_VT.end())
		{
			verify = false;
			break;
		}
	}

	//2. Verificam daca S apartine lui VN
	if (find(m_VN.begin(), m_VN.end(), m_S) == m_VN.end())
		verify = false;
	
	//3. Verificam daca pentru fiecare productie
	//   membrul stang contine cel putin un neterminal
	auto countVN = 0;

	for (auto itProd = m_P.begin(); itProd != m_P.end(); ++itProd)
	{
		for (auto it = itProd->first.begin(); it != itProd->first.end(); ++it)
			if (find(m_VN.begin(), m_VN.end(), *it) != m_VN.end())
				countVN++;
	}

	if (countVN < 1)
		verify = false;
	
	//4. Verificam daca exista cel putin o productie
	//   care are in staga doar S
	auto nProductions = 0;

	for (auto itProd = m_P.begin(); itProd != m_P.end(); ++itProd)
	{
		for (auto it = itProd->first.begin(); it != itProd->first.end(); ++it)
			if (*it == m_S)
				nProductions++;
	}

	if (nProductions < 1)
		verify = false;
	
	//5. Verificam daca fiecare productie
	//   contine doar elemente din VN si VT
	for (auto itProd = m_P.begin(); itProd != m_P.end(); ++itProd)
	{
		//V = VN U VT
		vector<char> V;
		V.reserve(m_VN.size() + m_VT.size());
		V = m_VN;
		V.insert(V.end(), m_VT.begin(), m_VT.end());
		
		//Membrul stang
		for(auto it = itProd->first.begin(); it != itProd->first.end(); ++it)
			if (find(V.begin(), V.end(), *it) == V.end())
				verify = false;

		//Membrul drept
		for (auto it = itProd->second.begin(); it != itProd->second.end(); ++it)
			if (find(V.begin(), V.end(), *it) == V.end())
				verify = false;
	}
	
	return verify;
}

Grammar::Production Grammar::getRandomProduction(vector<Production> productions)
{
	random_device rd;
	mt19937 g(rd());

	shuffle(productions.begin(), productions.end(), g);

	auto randomIndex = rand() % productions.size();

	return productions[randomIndex];
}

vector<Grammar::Production> Grammar::getApplicableProductions(string word)
{
	vector<Production> productions;

	for (auto itProd = m_P.begin(); itProd != m_P.end(); ++itProd)
	{
		if(word.find(itProd->first) != string::npos)
			productions.push_back(*itProd);
	}

	return productions;
}

bool Grammar::verifyWord(string word)
{
	for (auto letter = word.begin(); letter != word.end(); ++letter)
	{
		if (find(m_VN.begin(), m_VN.end(), *letter) != m_VN.end())
			return false;
	}

	return true;
}

void Grammar::generateWord(string word, const bool& option)
{
	//Retinerea posibilitatilor de aplicare a productiilor
	vector<Production> applicableProductions;

	//Generarea cuvintelor
	do
	{
		//Afisarea generarii cuvantului
		if (option)
			cout << word << " => ";

		applicableProductions.clear();

		//Calcularea productiilor aplicabile cuvantului curent
		applicableProductions = getApplicableProductions(word);
		
		if (!applicableProductions.empty())
		{
			//Alegerea random a unei productii aplicabile
			Production randomProduction = getRandomProduction(applicableProductions);

			//Aplicarea productiei
			auto index = word.find(randomProduction.first);
			word.reserve(word.size() + randomProduction.second.size());
			word.replace(index, randomProduction.first.size(), randomProduction.second);
		}	
		
	} while (!applicableProductions.empty());

	if (verifyWord(word))
		cout << "Cuvant corect!\n";

	//Afisarea cuvantului
	if(!option)
	{
		cout << "Cuvantul: ";

		for (auto it = word.begin(); it != word.end(); ++it)
			cout << *it;

	}
}

char Grammar::GetS() const
{
	return m_S;
}