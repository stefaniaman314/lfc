#include <iostream>

#include "Grammar.h"


int main()
{
	Grammar G;

	G.readFromFile("fileIn", 3, 3, 8);
	cout << "Gramatica Generativa G: \n\n";
	G.printGrammar();

	if (G.verifyGrammar())
	{
		cout << "\nGramatica este corect definita!\n";

		cout << "Cate cuvinte doriti sa generati?\n";
		int nWords;
		cin >> nWords;

		bool option = false;

		string word;
		word.push_back(G.GetS());

		for (auto index = 0; index < nWords; ++index)
		{
			G.generateWord(word, option);

			cout << '\n';
		}
	}

	else
		cout << "\nGramatica nu este corect definita!\n";
		
	system("pause");

	return 0;
}