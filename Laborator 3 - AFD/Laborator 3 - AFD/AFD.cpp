#include <iostream>
#include <fstream>

#include "AFD.h"

void AFD::readAFD(const string& fileName)
{
	ifstream myFile;
	myFile.open(fileName);

	while (!myFile.eof())
	{
		//Read states
		for (auto index = 0; index < kNumberOfStates; ++index)
		{
			string state;
			myFile >> state;
			m_states.push_back(state);
		}

		//Read input symbols
		for (auto index = 0; index < kNumberOfInputSymbols; ++index)
		{
			char symbol;
			myFile >> symbol;
			m_inputSymbols.push_back(symbol);
		}

		//Read initial state
		myFile >> m_initialState;

		//Read final states
		for (auto index = 0; index < kNumberOfFinalStates; ++index)
		{
			string state;
			myFile >> state;
			m_finalStates.push_back(state);
		}

		//Read transition function values
		for (auto index = 0; index < kNumberOfTransitions; ++index)
		{
			pair<string, char> left;
			string right;

			myFile >> left.first >> left.second >> right;

			TransitionFunction function(left, right);

			m_transitionFunction.push_back(function);
		}
	}

	myFile.close();
}

void AFD::printAFD()
{
	cout << " M = { ( ";

	for (auto index = 0; index < kNumberOfStates; ++index)
		cout << m_states[index] << " ";

	cout << "), ( ";

	for (auto index = 0; index < kNumberOfInputSymbols; ++index)
		cout << m_inputSymbols[index] << " ";

	cout << "), delta , " << m_initialState << ", ( ";

	for (auto index = 0; index < kNumberOfFinalStates; ++index)
		cout << m_finalStates[index] << " ";

	cout << ") } \n";

	cout << "\n Functia de tranzitie delta: \n\n";

	for (auto index = 0; index < kNumberOfTransitions; ++index)
		cout << m_transitionFunction[index] << '\n';
}

bool AFD::verifyWord(const string & word)
{
	for (auto symbol = word.begin(); symbol != word.end(); ++symbol)
		if (find(m_inputSymbols.begin(), m_inputSymbols.end(), *symbol) == m_inputSymbols.end())
			return false;
	
	return true;
}

string AFD::calculateState(const string & startState, const char & symbol)
{
	string endState;

	for (auto it = m_transitionFunction.begin(); it != m_transitionFunction.end(); ++it)
		if (it->getLeft().first == startState && it->getLeft().second == symbol)
			endState = it->getRight();

	return endState;
}

void AFD::verify(string word)
{
	string currentState;

	currentState = m_initialState;

	if (verifyWord(word))
	{
		for (auto symbol = word.begin(); symbol != word.end(); ++symbol)
		{
			currentState = calculateState(currentState, *symbol);

			if (currentState.empty())
			{
				cout << "Blocaj!\n";
				return;
			}
		}

		if (find(m_finalStates.begin(), m_finalStates.end(), currentState) != m_finalStates.end())
			cout << "Cuvant acceptat!\n";
		else
			cout << "Cuvant neacceptat!\n";
	}
	else
		cout << "Cuvantul nu apartine alfabetului!\n";
}