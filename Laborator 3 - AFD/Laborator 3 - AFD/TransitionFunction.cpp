#include "TransitionFunction.h"

TransitionFunction::TransitionFunction(const pair<string, char>& left, const string & right) :
	m_left(left),
	m_right(right)
{
	//
}

TransitionFunction::~TransitionFunction()
{
	//
}

 pair<string, char> TransitionFunction::getLeft()
{
	return m_left;
}

string TransitionFunction::getRight()
{
	return m_right;
}

ostream& operator<< (ostream& os, const TransitionFunction& obj)
{
	os << "(" << obj.m_left.first << ", " << obj.m_left.second << ") = " << obj.m_right;

	return os;
}
