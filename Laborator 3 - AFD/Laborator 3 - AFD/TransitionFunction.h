#pragma once

#include <string>

using namespace std;

class TransitionFunction
{
public:
	TransitionFunction() = default;
	TransitionFunction(const pair<string, char>& left, const string& right);
	~TransitionFunction();

	friend ostream& operator<< (ostream& os, const TransitionFunction& obj);

	pair<string, char> getLeft();
    string getRight();

private:
	pair<string, char> m_left;
	string m_right;
};

