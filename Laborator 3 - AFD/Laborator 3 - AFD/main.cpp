#include "AFD.h"
#include <iostream>
int main()
{
	AFD afd;

	afd.readAFD("in.txt");
	afd.printAFD();

	string word;

	while (true)
	{
		cout << "\nCuvantul: ";
		cin >> word;

		afd.verify(word);

		if (word == "quit")
			break;
	}

	return 0;
}