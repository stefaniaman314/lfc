#pragma once

#include "TransitionFunction.h"

#include <string>
#include <vector>
#include <unordered_map>

using namespace std;

class AFD
{
public:
	AFD() = default;

	//Citirea elementelor AFD-ului
	void readAFD(const string& fileName);

	//Printarea AFD-ului
	void printAFD();

	//Functie ajutatoare care calculeaza tranzitia
	//dint-o anumita stare cu un anumit simbol
	string calculateState(const string& state, const char& symbol);

	//Functie care verifica daca un cuvant esti format doar din 
	//simboluti acceptate
	bool verifyWord(const string& word);

	//Verifica daca cuvantul dat ca parametru 
	//este acceptat de AFD
	void verify(string word); 

private:
	vector<string> m_states;
	vector<char> m_inputSymbols;
	string m_initialState;
	vector<string> m_finalStates;
	vector<TransitionFunction> m_transitionFunction;

	constexpr static uint8_t kNumberOfStates = 6;
	constexpr static uint8_t kNumberOfInputSymbols = 3;
	constexpr static uint8_t kNumberOfFinalStates = 2;
	constexpr static uint8_t kNumberOfTransitions = 15;
};

