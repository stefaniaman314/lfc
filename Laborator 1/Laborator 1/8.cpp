#include <iostream>
#include <string>

int main()
{
	std::cout << "Enter two words: ";
	std::string s1, s2;
	std::cin >> s1 >> s2;

	if ((s1.compare(s1.length() - 2, 2, s2, s2.length() - 2, 2)) == 0)
		std::cout << "The words rhyme!\n";
	else
		std::cout << "The words don't rhyme.\n";

	system("pause");

	return 0;
}



