#include <iostream>
#include <regex>
#include <string>
using std::regex;
using std::string;
using std::sregex_token_iterator;

int main()
{
	regex re("[\\s?!]+");
	string s = "Ce cuvinte sunt in fraza ? Determina!!";
	sregex_token_iterator it(s.begin(), s.end(), re, -1);
	sregex_token_iterator reg_end;
	
	int count = 0;

	for (; it != reg_end; ++it) {

		count++;
		std::cout << count << ". " << *it << '\n';
	}

	system("pause");
	return 0;
}