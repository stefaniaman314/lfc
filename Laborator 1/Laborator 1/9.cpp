#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <algorithm>

using namespace std;

string getRandomWord(vector<string> words)
{
	std::random_device rd;
	std::mt19937 g(rd());

	std::shuffle(words.begin(), words.end(), g);

	auto randomIndex = rand() % words.size();

	return words[randomIndex];
}

void printWord(string word, int size)
{
	for (auto i = 0; i < size; ++i)
		cout << word[i] << " ";

	cout << '\n' << '\n';
}

int main()
{
	std::string const picture0 = "\n\n"
								 "   +----+  \n"
								 "   |    |  \n"
								 "   |       \n"
								 "   |       \n"
								 "   |       \n"
								 "   |       \n"
								 "  =============\n\n";

	std::string const picture1 = "\n\n"
								 "   +----+  \n"
								 "   |    |  \n"
								 "   |    O  \n"
								 "   |       \n"
								 "   |       \n"
								 "   |       \n"
								 "  =============\n\n";

	std::string const picture2 = "\n\n"
								 "   +----+  \n"
								 "   |    |  \n"
								 "   |    O  \n"
								 "   |    |  \n"
								 "   |       \n"
								 "   |       \n"
								 "  =============\n\n";

	std::string const picture3 = "\n\n"
								 "   +----+  \n"
								 "   |    |  \n"
								 "   |    O  \n"
								 "   |   /|  \n"
								 "   |       \n"
								 "   |       \n"
								 "  =============\n\n";

	std::string const picture4 = "\n\n"
								 "   +----+   \n"
								 "   |    |   \n"
								 "   |    O   \n"
								 "   |   /|\\ \n"
								 "   |        \n"
								 "   |        \n"
								 "  =============\n\n";

	std::string const picture5 = "\n\n"
								 "   +----+   \n"
								 "   |    |   \n"
								 "   |    O   \n"
								 "   |   /|\\ \n"
								 "   |   /    \n"
								 "   |        \n"
								 "  =============\n\n";

	std::string const picture6 = "\n\n"
								 "   +----+   \n"
								 "   |    |   \n"
								 "   |    O   \n"
								 "   |   /|\\ \n"
								 "   |   / \\ \n"
								 "   |        \n"
								 "  =============\n\n";

	vector<string> words = { "ACCOUNT","ACCURATE","ACRES","ACROSS","ACT","ACTION","ACTIVE","ACTIVITY",
							"ALL","ALLOW","ALMOST","ALONE","ALONG","ALOUD","ALPHABET","ALREADY","ALSO",
							"ALTHOUGH","AM","AMONG","AMOUNT","ANCIENT","ANGLE","ANGRY","ANIMAL","ANNOUNCED",
							"ANOTHER","ANSWER","ANTS","ANY","ANYBODY","ANYONE","ANYTHING","ANYWAY","ANYWHERE",
							"APART","APARTMENT","APPEARANCE","APPLE","APPLIED","APPROPRIATE","ARE","AREA","ARM",
							"ARMY","AROUND","ARRANGE","ARRANGEMENT","BUILD","BUILDING","BUILT","BURIED","BURN",
							"BURST","BUS","BUSH","BUSINESS","BUSY","BUT","BUTTER","BUY","BY","CABIN","CAGE","CAKE",
							"CALL","CALM","CAME","CAMERA","CAMP","CAN","CANAL","CANNOT","CAP","CAPITAL","CAPTAIN",
							"CAPTURED","CARBON","CARD","CAREFUL","CAREFULLY","CARRIED","CARRY","CASE","CAST","CASTLE" };

	string currentWord = getRandomWord(words);
	auto wordSize = currentWord.length();

	string underscore;
	underscore.reserve(wordSize);

	for (auto i = 0; i < wordSize; ++i)
		underscore.push_back('_');

	cout << "========== Guess the word! ========== \n";

	cout << picture0 << '\n';

	printWord(underscore, wordSize);

	cout << '\n' << '\n';

	const auto maxErrors = 6;
	auto errors = 0;

	while (underscore != currentWord && errors < maxErrors)
	{
		string letter;
		cout << "Enter a letter: ";
		cin >> letter;

		auto letterPos = currentWord.find(letter);

		if (letterPos == string::npos)
		{
			errors++;
			cout << "Wrong letter! You have " << maxErrors - errors << " mistakes left. \n";

			switch (errors)
			{
			case 0:
				cout << picture0;
				printWord(underscore, wordSize);
				break;

			case 1:
				cout << picture1;
				printWord(underscore, wordSize);
				break;

			case 2:
				cout << picture2;
				printWord(underscore, wordSize);
				break;

			case 3:
				cout << picture3;
				printWord(underscore, wordSize);
				break;
			
			case 4:
				cout << picture4;
				printWord(underscore, wordSize);
				break;

			case 5:
				cout << picture5;
				printWord(underscore, wordSize);
				break;

			case 6:
				cout << picture6;
				printWord(underscore, wordSize);
				break;

			default:
				break;
			}

		}
			
		else
		{
			underscore.replace(letterPos, 1, letter);  

			cout << "Correct! \n\n";

			printWord(underscore, wordSize);
		}

		
		if (errors >= maxErrors)
		{
			cout << '\n';
			cout << "========== GAME OVER! ========== \n";
			cout << '\n';
			cout << "You can do better than this...\n";
			cout << '\n';

			break;
		}

		if (underscore == currentWord)
		{
			cout << '\n';
			cout << "========== YOU WON! ========== \n";
			cout << '\n';
			cout << "You are very special. c: \n";
			cout << '\n';
		}
			
	}

	system("pause");

	return 0;
}