#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	string word;
	cout << "Enter word: ";
	cin >> word;

	vector<string> vect;

	int wordSize = word.length();

	for (auto i = 0; i < wordSize; ++i)
	{
		word.pop_back();
		vect.push_back(word);
	}

	reverse(vect.begin(), vect.end());

	for (auto elem : vect)
		cout << elem << '\n';
	
	system("pause");

	return 0;
}