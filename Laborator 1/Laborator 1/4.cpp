#include <iostream>
#include <stdio.h>
#include <string>
#include <algorithm>

void ReplaceAll(std::string& str, const std::string& from, const std::string& to)
{
	//A value of 0 means that the entire string is searched.
	size_t start_pos = 0;

	while ((start_pos = str.find(from, start_pos)) != std::string::npos) 
	{
		str.replace(start_pos, from.length(), to);
		start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
	}
}

int main()
{
	std::string str = "some text here and here";
	std::cout << "Initial string: " << str << '\n';

	//int size = str.size();
	//for (int i = 0; i < size / 2; i++)
		//std::swap(str[i], str[size - i - 1]);
	
	reverse(str.begin(), str.end());

	std::cout << "Reversed string: " << str << '\n';

	const std::string s1 = "ereh";
	const std::string s2 = "there";

	//Inlocuieste in sirul str toate aparitiile subsirului s1 cu s2.
	ReplaceAll(str, s1, s2);
	
	std::cout << "Final string: " << str << '\n';

	system("pause");

	return 0;
}