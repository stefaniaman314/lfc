﻿#include <iostream>
#include <stdio.h>
#include <string>
#include <algorithm>

//Având un cuvânt x și un alfabet V să se verifice dacă x este cuvânt peste V (adică toate literele din x se regăsesc în V).

bool isWord(const std::string& x, const std::string& V)
{
	bool verify = false;

	for (auto i = 0; i < x.size(); ++i)
	{
		if (V.find(x[i]) != std::string::npos)
			verify = true;
		else
		{
			verify = false;
			break;
		}
	}

	return verify;
}

int main()
{
	std::string V = { 'a', 'b', 'c', 'd' };
	std::cout << "Vocabulary: " << V << '\n';

	std::string x = "abbcccdddd";
	std::cout << "Word: " << x << '\n';

	if (isWord(x, V))
		std::cout << "x este cuvant peste V \n";
	else
		std::cout << "x nu este cuvant peste V \n";

	system("pause");

	return 0;
}