#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include <algorithm>

using namespace std;

void sortAscendingOrder(int n)
{
	string word;
	vector<string> vect;

	for (auto i = 0; i < n; i++)\
	{
		cin >> word;
		vect.push_back(word);
	}

	sort(vect.begin(), vect.end());

	cout << "Sorted words: ";

	for (auto elem : vect)
		cout << elem << " ";

	cout << '\n';
}

/*void printReverseOrder(int n)
{
	string word;
	stack<string> s;

	for (auto i = 0; i < n; i++)
	{
		cin >> word;
		s.push(word);
	}

	cout << "Reverse order: ";

	while (!s.empty())
	{
		cout << s.top() << " ";
		s.pop();
	}

	cout << '\n';
}
*/

void printReverseOrder(int n)
{
	if (n == 0)
		return;

	string word;
	cin >> word; 
	
	printReverseOrder(n - 1);

	cout << word << " ";
}

int main()
{
	int n;
	cout << "n = ";
	cin >> n;

	sortAscendingOrder(n);
	printReverseOrder(n);

	cout << '\n';

	system("pause");

	return 0;
}