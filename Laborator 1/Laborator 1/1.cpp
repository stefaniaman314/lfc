#include <iostream>
#include <stdio.h>
#include <string>

int main()
{
	std::string myString = "Some text here";
	std::cout << myString << '\n';

	char ch;
	std::cout << "ch = ";
	std::cin >> ch;

	int count = 0;

	for (int i = 0; i < myString.size(); ++i)
		if (myString[i] == ch)
			count++;

	std::cout << "Caracterul '" << ch << "' apare de " << count << " ori in sir. \n";

	system("pause");

	return 0;
}